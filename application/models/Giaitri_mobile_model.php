<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 5/12/17
 * Time: 09:06
 */
class Giaitri_mobile_model extends CI_Model
{
    /**
     * Giaitri_mobile_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->db          = $this->load->database('scan_url_giaitri_mobile', TRUE, TRUE);
        $this->primary_key = 'id';
        $this->is_not      = ' !=';
        $this->or_higher   = ' >=';
        $this->is_higher   = ' >';
        $this->or_smaller  = ' <=';
        $this->is_smaller  = ' <';
        $this->start_time  = ' 00:00:00';
        $this->end_time    = ' 23:59:59';
    }
    /**
     * check_exists
     *
     * @access      public
     * @author 		Hung Nguyen <dev@nguyenanhung.com>
     * @version     1.0.1
     * @since       22/03/2017
     */
    public function check_exists($value = '', $field = null)
    {
        $this->db->select('id');
        $this->db->from($this->tableName);
        if ($field === null)
        {
            $this->db->where($this->primary_key, $value);
        }
        else
        {
            $this->db->where($field, $value);
        }
        return (int) $this->db->count_all_results();
    }
    /**
     * get_info
     *
     * @access      public
     * @author 		Hung Nguyen <dev@nguyenanhung.com>
     * @version     1.0.1
     * @since       22/03/2017
     */
    public function get_info($value = '', $field = null, $array = false)
    {
        $this->db->from($this->tableName);
        if ($field === null)
        {
            $this->db->where($this->primary_key, $value);
        }
        else
        {
            $this->db->where($field, $value);
        }
        if ($array === true)
        {
            return $this->db->get()->row_array();
        }
        else
        {
            return $this->db->get()->row();
        }
    }
    /**
     * get_value
     *
     * @access      public
     * @author 		Hung Nguyen <dev@nguyenanhung.com>
     * @version     1.0.1
     * @since       22/03/2017
     */
    public function get_value($value_input = '', $field_input = null, $field_output = null)
    {
        if (null !== $field_output)
        {
            $this->db->select($field_output);
        }
        $this->db->from($this->tableName);
        if ($field_input === null)
        {
            $this->db->where($this->primary_key, $value_input);
        }
        else
        {
            $this->db->where($field_input, $value_input);
        }
        $query = $this->db->get();
        // Query
        if (null !== $field_output)
        {
            if (null === $query->row())
            {
                return null;
            }
            else
            {
                return $query->row()->$field_output;
            }
        }
        else
        {
            return $query->row();
        }
    }
    /**
     * add
     *
     * @access      public
     * @author 		Hung Nguyen <dev@nguyenanhung.com>
     * @version     1.0.1
     * @since       22/03/2017
     * @var add new Item to DB
     */
    public function add($data = array())
    {
        $this->db->insert($this->tableName, $data);
        return $this->db->insert_id();
    }
    /**
     * update
     *
     * @access      public
     * @author 		Hung Nguyen <dev@nguyenanhung.com>
     * @version     1.0.1
     * @since       22/03/2017
     */
    public function update($table, $id = '', $data = array())
    {
        $this->db->where($this->primary_key, $id);
        $this->db->update($table, $data);
        return $this->db->affected_rows();
    }
    /**
     * delete
     *
     * @access      public
     * @author 		Hung Nguyen <dev@nguyenanhung.com>
     * @version     1.0.1
     * @since       22/03/2017
     */
    public function delete($id = '')
    {
        if (empty($id))
        {
            return false;
        }
        $this->db->where($this->primary_key, $id);
        $this->db->delete($this->tableName);
        return $this->db->affected_rows();
    }

    public function get_result($table, $input_value = null, $field = null)
    {
        $this->db->from($table);
        $this->db->not_like($field, $input_value);
            // Genarate result
        return $this->db->get()->result();
    }
}
/* End of file Giaitri_mobile_model.php */
/* Location: ./application/model/Giaitri_mobile_model.php */

