<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Cocur\Slugify\Slugify;
class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function tools($table, $field)
	{
        $this->load->helper('file');
        $slugify = new Slugify();
        // load model
	    $this->load->model('giaitri_mobile_model');
        // get list result to scan
        $list_result = $this->giaitri_mobile_model->get_result($table, 'cms.giaitrivn.net', $field);
        $output = '';
        $op_db_bongda = 'http://cms.giaitrivn.net/public/uploads/clip_bongda/';
        $op_db_cliphot = 'http://cms.giaitrivn.net/public/uploads/clip_hot/';
        $op_db_music = 'http://cms.giaitrivn.net/public/uploads/musics/';
        $begin = 'wget --output-document=';
        foreach ($list_result as $key => $value)
        {
            $ext = pathinfo($value->$field, PATHINFO_EXTENSION);
            $filename = pathinfo($value->$field, PATHINFO_FILENAME);
            $filename_decode = urldecode($filename);
            $slugname = $slugify->slugify($filename_decode);
            $output .= $begin . $slugname . '.' . $ext . ' ' . $value->$field . " && ";
            if ($table == 'clipbongda')
            {
                $op_to_db = $op_db_bongda . $slugname . '.' . $ext;
                $this->giaitri_mobile_model->update($table, $value->id, array($field => $op_to_db));
            }
            else if ($table == 'cliphot')
            {
                $op_to_db = $op_db_cliphot . $slugname . '.' . $ext;
                $this->giaitri_mobile_model->update($table, $value->id, array($field => $op_to_db));
            }
            else
            {
                $op_to_db = $op_db_music . $slugname . '.' . $ext;
                $this->giaitri_mobile_model->update($table, $value->id, array($field => $op_to_db));
            }
        }
        $output_replaced = str_replace("(", "\(", $output);
        $output_replaced = str_replace(")", "\)", $output_replaced);
        if (!write_file('publish_folder/' . $table . '.txt', $output_replaced))
        {
            echo 'Unable to write the file';
        }
        else
        {
            echo 'File written!';
        }
	}
}
